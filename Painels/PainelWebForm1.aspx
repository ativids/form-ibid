﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PainelWebForm1.aspx.cs" Inherits="Painels.PainelWebForm1" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Painéis com Estilo</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .custom-panel {
            border: 2px solid #007BFF;
            padding: 20px;
            border-radius: 10px;
            background-color: #f8f8f8;
            width: 300px;
            text-align: center;
        }

        .section-title {
            font-size: 18px;
            margin-bottom: 15px;
        }

        .custom-textbox {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .custom-button {
            padding: 10px 20px;
            background-color: #007BFF;
            color: #fff;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        .custom-button:hover {
            background-color: #0056b3;
        }

        .mensagem {
            color: #ff0000; /* Cor vermelha para mensagens de erro */
        }

        .mensagem-sucesso {
            color: #008000; /* Cor verde para mensagens de sucesso */
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pnlInfoPessoais" runat="server" Visible="true" CssClass="custom-panel">
            <h2 class="section-title">INFORMAÇÕES PESSOAIS</h2>
            Nome: <asp:TextBox ID="txtNome" runat="server" CssClass="custom-textbox" placeholder="Seu nome"></asp:TextBox><br />
            Sobrenome: <asp:TextBox ID="txtSobrenome" runat="server" CssClass="custom-textbox" placeholder="Seu sobrenome"></asp:TextBox><br />
            Gênero: <asp:TextBox ID="txtGenero" runat="server" CssClass="custom-textbox" placeholder="Seu gênero"></asp:TextBox><br />
            Celular: <asp:TextBox ID="txtCelular" runat="server" CssClass="custom-textbox" placeholder="Seu celular"></asp:TextBox><br />
            <asp:Button ID="btnProximo1" runat="server" Text="Próximo" OnClick="btnProximo1_Click" CssClass="custom-button" />
        </asp:Panel>

        <asp:Panel ID="pnlEndereco" runat="server" Visible="false" CssClass="custom-panel">
            <h2 class="section-title">Detalhes do Endereço</h2>
            Endereço: <asp:TextBox ID="txtEndereco" runat="server" CssClass="custom-textbox" placeholder="Seu endereço"></asp:TextBox><br />
            Cidade: <asp:TextBox ID="txtCidade" runat="server" CssClass="custom-textbox" placeholder="Sua cidade"></asp:TextBox><br />
            CEP: <asp:TextBox ID="txtCEP" runat="server" CssClass="custom-textbox" placeholder="Seu CEP"></asp:TextBox><br />
            <asp:Button ID="btnVoltar" runat="server" Text="Voltar" OnClick="btnVoltar_Click" CssClass="custom-button" />
            <asp:Button ID="btnProximo2" runat="server" Text="Próximo" OnClick="btnProximo2_Click" CssClass="custom-button" />
        </asp:Panel>

        <asp:Panel ID="pnlLogin" runat="server" Visible="false" CssClass="custom-panel">
            <h2 class="section-title">Área de Login</h2>
            Usuário: <asp:TextBox ID="txtUsuario" runat="server" CssClass="custom-textbox" placeholder="Nome de usuário"></asp:TextBox><br />
            Senha: <asp:TextBox ID="txtSenha" runat="server" TextMode="Password" CssClass="custom-textbox" placeholder="Sua senha"></asp:TextBox><br />
            <asp:Button ID="btnVoltar2" runat="server" Text="Voltar" OnClick="btnVoltar2_Click" CssClass="custom-button" />
            <asp:Button ID="btnEnviar" runat="server" Text="Enviar" OnClick="btnEnviar_Click" CssClass="custom-button" />
            <asp:Label ID="lblMensagem" runat="server" Text="" CssClass="mensagem"></asp:Label>
        </asp:Panel>
    </form>
</body>
</html>