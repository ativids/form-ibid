﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Painels
{
    public partial class PainelWebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnProximo1_Click(object sender, EventArgs e)
        {
            pnlInfoPessoais.Visible = false;  
            pnlEndereco.Visible = true;  
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
          
            pnlEndereco.Visible = false;  
            pnlInfoPessoais.Visible = true;  
        }

        protected void btnProximo2_Click(object sender, EventArgs e)
        {
            
            pnlEndereco.Visible = false; 
            pnlLogin.Visible = true; 
        }

        protected void btnVoltar2_Click(object sender, EventArgs e)
        {
            pnlLogin.Visible = false;  
            pnlEndereco.Visible = true;  
        }

      
        protected void btnEnviar_Click(object sender, EventArgs e)
        {

            lblMensagem.Text = "AVISO! Os seus dados foram enviados com sucesso";
            lblMensagem.CssClass = "mensagem-sucesso";
        }




    }
}